#include "Board.hpp"

#include <iostream>

Board::Board(const char* initString) {
    int i      = 0;
    int cursor = 0;

    while (initString[cursor] != 0) {
        char next = initString[cursor];

        if (next == '-') {
            data[i].value      = 0;
            data[i].isInit     = false;
            data[i].isConflict = false;
            ++i;
        }
        else if ('1' <= next && next <= '9') {
            data[i].value      = next - '0';
            data[i].isInit     = true;
            data[i].isConflict = false;
            ++i;
        }

        if (i >= 81) break;

        ++cursor;
    }
}

bool Board::validate() {
    bool ret = true;

    // Rows
    for (int row = 0; row < 9; ++row) {
        for (int i = 1; i <= 9; ++i) {
            int first = -1;

            for (int col = 0; col < 9; ++col) {
                int index = 9 * row + col;
                if (i != data[index].value) continue;

                if (first < 0) {
                    first = index;
                }
                else {
                    data[index].isConflict = true;
                    data[first].isConflict = true;
                    ret = false;
                }
            }
        }
    }

    // Cols
    for (int col = 0; col < 9; ++col) {
        for (int i = 1; i <= 9; ++i) {
            int first = -1;

            for (int row = 0; row < 9; ++row) {
                int index = 9 * row + col;
                if (i != data[index].value) continue;

                if (first < 0) {
                    first = index;
                }
                else {
                    data[index].isConflict = true;
                    data[first].isConflict = true;
                    ret = false;
                }
            }
        }
    }

    // Blocks
    for (int block = 0; block < 9; ++block) {
        for (int i = 1; i <= 9; ++i) {
            int first = -1;

            for (int pos = 0; pos < 9; ++pos) {
                int row = (block / 3) * 3 + (pos / 3);
                int col = (block % 3) * 3 + (pos % 3);

                int index = row * 9 + col;
                if (i != data[index].value) continue;

                if (first < 0) {
                    first = index;
                }
                else {
                    data[index].isConflict = true;
                    data[first].isConflict = true;
                    ret = false;
                }
            }
        }
    }

    return ret;
}

bool Board::checkCell(int targetIndex) {
    return checkCell(targetIndex % 9, targetIndex / 9);
}

bool Board::checkCell(int targetCol, int targetRow) {
    int targetIndex = targetRow * 9 + targetCol;
    int targetBlock = (targetRow / 3) * 3 + (targetCol / 3);
    int targetValue = data[targetIndex].value;

    if (targetValue < 1 || targetValue > 9) return true;

    // Row
    for (int col = 0; col < 9; ++col) {
        int index = targetRow * 9 + col;
        if (index == targetIndex) continue;

        if (data[index].value == targetValue) return false;
    }

    // Col
    for (int row = 0; row < 9; ++row) {
        int index = row * 9 + targetCol;
        if (index == targetIndex) continue;

        if (data[index].value == targetValue) return false;
    }

    // Block
    for (int pos = 0; pos < 9; ++pos) {
        int row = (targetBlock / 3) * 3 + (pos / 3);
        int col = (targetBlock % 3) * 3 + (pos % 3);

        int index = row * 9 + col;
        if (index == targetIndex) continue;

        if (data[index].value == targetValue) return false;
    }

    return true;
}

bool Board::solve() {
    int i = 0;
    for (;;)
    {
        // Find an empty cell
        while (i < 81 && data[i].isInit) ++i;
        if (i >= 81) return true;

        // Increase the value
        data[i].value += 1;

        // If we went over, backtrack
        if (data[i].value > 9) {
            data[i].value = 0;

            --i;
            while (i >= 0 && data[i].isInit) --i;
            if (i < 0) return false;
        }
        // If the new value doesnt cause a conflict, find the next cell
        else if (checkCell(i)) {
            ++i;
        }
    }
}

std::ostream& operator<<(std::ostream& out, const Board& board) {
    out << Board::firstRow << std::endl;

    for (int i = 0; i < 9; ++ i) {
        if (i != 0) out << ((i % 3 == 0) ? Board::majorRowDiv : Board::minorRowDiv) << std::endl;

        for (int j = 0; j < 9; ++j) {
            out << ((j % 3 == 0) ? Board::majorColDiv : Board::minorColDiv);
            out << board.data[i * 9 + j];
        }
        out << Board::majorColDiv << std::endl;
    }

    return out << Board::lastRow;
}

std::ostream& operator<<(std::ostream& out, const Board::Cell& cell) {
    out << ((cell.isConflict) ? "*" : " ");
    if (cell.isInit) out << "\e[1;31m"; // Bold on

    if (1 <= cell.value && cell.value <= 9) out << cell.value;
    else                                    out << " ";

    if (cell.isInit) out << "\e[0m"; // Bold off
    out << ((cell.isConflict) ? "*" : " ");

    return out;
}
