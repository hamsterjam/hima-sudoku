#include <iostream>

#include "Board.hpp"
#include "ArgParser.hpp"

void display(Board& board) {
    std::cout << board << std::endl;
}

void solve(Board& board) {
    bool solved = board.solve();
    std::cout << board << std::endl;
    std::cout << (solved ? "Solved" : "No solution") << std::endl;
}

void verify(Board& board) {
    bool verify = board.validate();
    std::cout << board << std::endl;
    std::cout << (verify ? "No errors" : "Errors found") << std::endl;
}

int main(int argc, char** argv) {
    ArgParser args(argc, argv);
    Board board(args.getInitString());

    switch (args.getMode()) {
        case ArgParser::Mode::Display:
            display(board);
            break;

        case ArgParser::Mode::Solve:
            solve(board);
            break;

        case ArgParser::Mode::Verify:
            verify(board);
            break;

        default:
            // Error
            break;
    }
    return 0;
}
