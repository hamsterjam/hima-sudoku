.DEFAULT_GOAL := debug

TITLE  := sudoku
OBJDIR := object
CFLAGS := -g -std=c++17

NAMES  := Board
NAMES  += ArgParser

SOURCES = $(addsuffix .cpp, $(NAMES))
OBJECTS = $(addprefix $(OBJDIR)/, $(addsuffix .o, $(NAMES)))

.PHONY: debug
debug: $(OBJECTS) main.cpp
	g++ $(CFLAGS) main.cpp $(OBJECTS) -o $(TITLE)

.PHONY: clean
clean: $(OBJDIR)
	rm -r $(OBJDIR)
	rm $(TITLE)*

$(OBJDIR):
	mkdir $(OBJDIR)

$(OBJDIR)/%.o: %.cpp $(OBJDIR)
	g++ $(CFLAGS) -c $< -o $@
