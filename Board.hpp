#ifndef HIMA_SUDOKU_BOARD_HPP_DEFINED
#define HIMA_SUDOKU_BOARD_HPP_DEFINED

#include <iostream>

class Board {
    public:
        struct Cell {
            int value;
            bool isInit;
            bool isConflict;
        };

    private:
        friend std::ostream& operator<<(std::ostream&, const Board&);

        static constexpr const char* firstRow    = "╔═══╤═══╤═══╦═══╤═══╤═══╦═══╤═══╤═══╗";
        static constexpr const char* lastRow     = "╚═══╧═══╧═══╩═══╧═══╧═══╩═══╧═══╧═══╝";
        static constexpr const char* minorRowDiv = "╟───┼───┼───╫───┼───┼───╫───┼───┼───╢";
        static constexpr const char* majorRowDiv = "╠═══╪═══╪═══╬═══╪═══╪═══╬═══╪═══╪═══╣";

        static constexpr const char* minorColDiv = "│";
        static constexpr const char* majorColDiv = "║";

        Cell data[81] = {0};

    public:
        Board() {}
        Board(const char* initString);

        bool validate();
        bool checkCell(int targetIndex);
        bool checkCell(int targetCol, int targetRow);

        bool solve();
};

std::ostream& operator<<(std::ostream& out, const Board& board);
std::ostream& operator<<(std::ostream& out, const Board::Cell& cell);

#endif
