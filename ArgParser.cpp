#include "ArgParser.hpp"

#include <cstring>

ArgParser::ArgParser(int argc, char** argv) {
    this->argc = argc;
    this->argv = argv;
    pos = 1;

    setDefaults();

    while (pos < argc) {
        const char* curr = argv[pos];

        if (curr[0] == '-' && curr[1] == '-') {
            eatLongFlag();
        }
        else if (curr[0] == '-') {
            eatShortFlag();
        }
        else {
            eatBoardState();
        }
    }
}

void ArgParser::setDefaults() {
    initString = "";
    mode = Mode::Display;
}

void ArgParser::eatShortFlag() {
    char flag = argv[pos][1];
    if ('1' <= flag && flag <= '9') {
        eatBoardState();
        return;
    }

    switch (flag) {
        case 'e':
            initString = exampleBoard;
            break;

        case 's':
            mode = Mode::Solve;
            break;

        case 'v':
            mode = Mode::Verify;
            break;

        default :
            // Error
            break;
    }

    ++pos;
}

void ArgParser::eatLongFlag() {
    const char* flag = argv[pos] + 2;

    if (flag[0] == '-' || '1' <= flag[0] && flag[0] <= '9') {
        eatBoardState();
        return;
    }

    if (strcmp(flag, "example") == 0) {
        initString = exampleBoard;
    }
    else
    if (strcmp(flag, "solve") == 0) {
        mode = Mode::Solve;
    }
    else
    if (strcmp(flag, "verify") == 0) {
        mode = Mode::Verify;
    }
    else {
        // Error
    }

    ++pos;
}

void ArgParser::eatBoardState() {
    initString = argv[pos];
    ++pos;
}

const char* ArgParser::getInitString() {
    return initString;
}

ArgParser::Mode ArgParser::getMode() {
    return mode;
}
