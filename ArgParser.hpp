#ifndef HIMA_SUDOKU_ARG_PARSER_HPP_DEFINED
#define HIMA_SUDOKU_ARG_PARSER_HPP_DEFINED

class ArgParser {
    public:
        enum class Mode {
            Display,
            Solve,
            AnimatedSolve,
            Verify,
        };

    private:
        int    argc;
        char** argv;
        int    pos;

        const char* initString;
        Mode mode;

    public:
        ArgParser(int argc, char** argv);

        static constexpr const char* exampleBoard = R"(
            - - -  6 - -  - - -
            - - 6  - 7 -  5 - -
            - 3 -  - - 9  - 4 -

            - - 1  - 8 -  - - 3
            - 4 -  7 - 2  - 8 -
            7 - -  - 5 -  4 - -

            - 9 -  2 - -  - 1 -
            - - 7  - 6 -  8 - -
            - - -  - - 3  - - -
        )";

        const char* getInitString();
        Mode getMode();

    private:
        void setDefaults();

        void eatShortFlag();
        void eatLongFlag();
        void eatBoardState();
};

#endif
